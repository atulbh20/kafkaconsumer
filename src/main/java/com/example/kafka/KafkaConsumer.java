package com.example.kafka;

import java.util.concurrent.CountDownLatch;

import org.springframework.kafka.annotation.KafkaListener;

public class KafkaConsumer {
	
	private CountDownLatch latch = new CountDownLatch(1);
	private long startTs;
	private long endTs;
	
	//@KafkaListener(topicPartitions={@TopicPartition(topic = "test2", partitions={"1"})})
	@KafkaListener(id = "${kafka.consumer.groupId}", topics = "${kafka.topic.name}")
	public void receiveMessage(String message) {
    	//System.out.printf("received message =%s, timestamp=%s \n", message, System.currentTimeMillis());
		if("11111".equals(message)){
    		System.out.println("Consumer Started!!");
    		startTs = System.currentTimeMillis();
    	}
    	if("22222".equals(message)){
    		endTs = System.currentTimeMillis();
    		System.out.println("Total time taken to receive: " + (endTs - startTs) );
    	}
		latch.countDown();
    }
    
    /*@KafkaListener(topicPartitions={@TopicPartition(topic = "test2", partitions={"2"})})
    public void receiveMessage2(String message) {
    	System.out.printf("received message on partition 2='%s'\n", message);
        latch.countDown();
    }

    @KafkaListener(topicPartitions={@TopicPartition(topic = "test2", partitions={"0"})})
    public void receiveMessage3(String message) {
    	System.out.printf("received message on partition 0='%s'\n", message);
        latch.countDown();
    }*/
    
    public CountDownLatch getLatch() {
        return latch;
    }
}
